package engine;

import model.Node;

public class EngineResponse {
	private EngineResponseEnum engineResponseEnum;
	private Node node;

	public EngineResponse(EngineResponseEnum engineResponseEnum, Node node) {
		super();
		this.engineResponseEnum = engineResponseEnum;
		this.node = node;
	}

	public EngineResponse(EngineResponseEnum engineResponseEnum) {
		super();
		this.engineResponseEnum = engineResponseEnum;
	}

	public EngineResponseEnum getEngineResponseEnum() {
		return engineResponseEnum;
	}

	public void setEngineResponse(EngineResponseEnum engineResponseEnum) {
		this.engineResponseEnum = engineResponseEnum;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	@Override
	public String toString() {
		String s = engineResponseEnum.toString();
		s += " " + node.toString();
		return s;

	}

}
