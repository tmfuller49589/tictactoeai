package engine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.Node;
import model.PlayerEnum;

public class EngineMiniMax extends EngineAbstract {

	protected static final Logger logger = LogManager.getLogger(EngineMiniMax.class);

	private final int maxScore = 10;
	private final int minScore = -maxScore;

	public EngineMiniMax() {
	}

	@Override
	public void init() {
		super.init();
		if (firstPlayer == PlayerEnum.COMPUTER) {
			minimax(headNode, 0, true);
		} else {
			minimax(headNode, 0, false);
		}
	}

	@Override
	void computerMove() {

		Node best = null;
		for (Node child : lastMove.getChildNodes()) {
			if (best == null) {
				best = child;
			} else if (child.getValue() > best.getValue()) {
				best = child;
			}
		}
		lastMove = best;
		computerMove = best;
	}

	int minimax(Node node, int depth, Boolean isMaximizingPlayer) {
		if (node.getWinner() == PlayerEnum.COMPUTER) {
			node.setValue(maxScore);
			return maxScore;
		} else if (node.getWinner() == PlayerEnum.PLAYER) {
			node.setValue(minScore);
			return minScore;
		} else if (node.getWinner() == PlayerEnum.DRAW) {
			node.setValue(0);
			return 0;
		}

		if (isMaximizingPlayer) {
			// maximizer's move
			int best = Integer.MIN_VALUE;
			for (Node child : node.getChildNodes()) {
				int mm = minimax(child, depth - 1, !isMaximizingPlayer);
				best = Math.max(best, mm);
			}
			node.setValue(best);
			return best;
		} else {
			// minimizer's move
			int best = Integer.MAX_VALUE;
			for (Node child : node.getChildNodes()) {
				int mm = minimax(child, depth - 1, !isMaximizingPlayer);
				best = Math.min(best, mm);
			}
			node.setValue(best);
			return best;
		}
	}
}
