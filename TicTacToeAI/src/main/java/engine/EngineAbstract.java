package engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.Coord;
import model.Node;
import model.PlayerEnum;

/*
 * Class to play tic tac toe.
 * Board information is stored in a node.
 * Node contains a parent node and a list of child nodes.
 * 
 * All possible games are computed.  Every move and associated board position
 * is stored in a node.  By following the node children links, every possible
 * game can be replayed.
 * 
 * To avoid losing, we have to search through the tree and:
 * 1) make a forced move if required
 * 2) avoid making a move that leads to a position where two different moves can be made
 * to win the game.
 * 
 * With similar logic, computer will attempt to win if possible.
 * 
 */
public abstract class EngineAbstract {

	/*
	 * Logger is a class for handling debugging and error messages.
	 * You can set the log level in the file src/main/resources/log4j2.xml,
	 * e.g. change
	 * <Logger name="tictactoe" level="info" additivity="false">
	 * to
	 * <Logger name="tictactoe" level="debug" additivity="false">
	 * to display debugging info.
	 */
	protected static final Logger logger = LogManager.getLogger(EngineAbstract.class);

	protected int boardSize = 3;
	protected Coord[][] boardCoords;
	protected int numberOfGames = 0;
	protected Node headNode = null; // the top of the game tree
	protected Node playerMove = null; // a node for the player's move
	protected Node computerMove = null; // a node for the computer's move
	protected Node lastMove = headNode; // a node for the last move made

	/* This is a map.
	 * Every node is given an identifier.  We can retrieve the node in the map using
	 * the identifier.
	 * e.g.:
	 * Node node = nodeMap.get(1234);
	 * nodeMap is not needed by the algorithm logic.
	 * nodeMap is useful for debugging if a particular board position
	 * needs to be examined.
	 */
	protected Map<Integer, Node> nodeMap = new HashMap<Integer, Node>();

	protected PlayerEnum firstPlayer;

	// private Random randGen = new Random(5498);
	protected Random randGen = new Random(1);

	abstract void computerMove();

	public EngineAbstract() {
	}

	public void init() {
		Node.setNodeCount(0);
		headNode = new Node();
		lastMove = headNode;

		boardCoords = new Coord[boardSize][boardSize];
		for (int i = 0; i < boardSize; ++i) {
			boardCoords[i] = new Coord[boardSize];
			for (int j = 0; j < boardSize; ++j) {
				Coord coord = new Coord(i, j);
				boardCoords[i][j] = coord;
			}
		}

		// randomly decide who goes first
		playerMove = null;
		computerMove = null;
		if (randGen.nextBoolean()) {
			firstPlayer = PlayerEnum.PLAYER;
			headNode.setPlayer(PlayerEnum.COMPUTER);
		} else {
			firstPlayer = PlayerEnum.COMPUTER;
			headNode.setPlayer(PlayerEnum.PLAYER);
		}

		System.out.println(firstPlayer + " goes first");
		System.out.println(PlayerEnum.COMPUTER + " is " + PlayerEnum.COMPUTER.getToken());
		System.out.println(PlayerEnum.PLAYER + " is " + PlayerEnum.PLAYER.getToken());

		nextMove(headNode);

		System.out.println("total number of games computed is " + numberOfGames);

		nodeMap.clear();
		addNodesToHashMap(headNode);

		System.out.println("nodes hash map size = " + nodeMap.size());

	}

	public EngineResponse startGame() {
		init();
		if (firstPlayer == PlayerEnum.COMPUTER) {
			computerMove();
			return new EngineResponse(EngineResponseEnum.COMPUTERFIRST, computerMove);
		}

		return new EngineResponse(EngineResponseEnum.PLAYERFIRST, headNode);
	}

	/*
	 * This method will compute all possible games.
	 * Recursive method to make next move.
	 * For each child node that this node has,
	 * recursively call nextMove which will make all possible
	 * moves for the child node.  
	 */
	private void nextMove(Node node) {

		PlayerEnum winner = checkWinner(node);
		if (winner == PlayerEnum.COMPUTER || winner == PlayerEnum.PLAYER) {
			// someone has won
			++numberOfGames;
			node.setWinner(winner);
			return;
		}

		// no one has won, carry on
		List<Coord> availCoords = getAvailableCoords(node);

		if (availCoords.size() == 0) {
			++numberOfGames;
			node.setWinner(PlayerEnum.DRAW);
			return;
		}

		// this switches the players
		PlayerEnum current;
		if (node.getPlayer() == PlayerEnum.PLAYER) {
			current = PlayerEnum.COMPUTER;
		} else {
			current = PlayerEnum.PLAYER;
		}

		// loop through the available coords,
		// make all possible moves for each available coord
		// by recursively calling nextMove
		for (Coord coord : availCoords) {
			Node newNode = new Node(node, current, coord);
			nextMove(newNode);
		}
	}

	/*
	 * Add all the nodes to a hash map.  The hash map is not necessary for the algorithm to function.
	 * The hash map is useful for debugging as it allows a node to be retrieved easily
	 * given the node number - e.g.
	 * Node node = nodeHashMap.get(1234);
	 */
	private void addNodesToHashMap(Node node) {
		nodeMap.put(node.getNodeNumber(), node);
		for (Node child : node.getChildNodes()) {
			addNodesToHashMap(child);
		}
	}

	/*
	 * Start the game.
	 */
	private void run() {
		logger.debug("head node is " + headNode);

		PlayerEnum winner;

		boolean firstMove = true;
		do {
			/*
			 *  Call get player move if:
			 *  1) we are on the first move and player goes first.
			 *  2) we are not on the first move.
			 *  Otherwise, skip the call to get player move and
			 *  let computer go first.
			 */
			if (firstMove && firstPlayer == PlayerEnum.PLAYER || !firstMove) {
				// getPlayerMove();
			}

			logger.debug("last move is " + lastMove + System.lineSeparator() + "+++++++++++++++++++++");

			winner = checkWinner(lastMove);

			// If player did not win, computer gets to move
			if (winner == PlayerEnum.NOWINNER) {
				computerMove();
			}
			firstMove = false;

			printBoard(lastMove);
			winner = checkWinner(lastMove);

		} while (getAvailableCoords(headNode).size() > 0 && winner == PlayerEnum.NOWINNER);

		System.out.println("winner is -->" + winner + "<--");
		System.out.println("game over");
	}

	/*
	 * 
	 * ============================================================================================================
	 * Code in this section is the logic for playing TicTacToe.
	 * ============================================================================================================
	 * 
	 */

	/*
	 * Starting from node, follow the tree and return a 
	 * list of all possible ending nodes that are descendants
	 * of node.
	 */
	void getTerminalNodes(Node node, List<Node> nodes) {
		if (node.getChildNodes().size() == 0) {
			nodes.add(node);
			return;
		}

		for (Node child : node.getChildNodes()) {
			getTerminalNodes(child, nodes);
		}
	}

	/*
	 * Check if we have a winner.
	 */
	private PlayerEnum checkWinner(Node startNode) {
		char[][] board = generateBoard(startNode);

		// check rows
		int tally;
		char playerToken;
		for (int row = 0; row < boardSize; ++row) {
			tally = 1;
			playerToken = board[row][0];
			if (playerToken == ' ') {
				continue;
			}
			for (int col = 1; col < boardSize; ++col) {
				if (board[row][col] == playerToken) {
					++tally;
				}
			}
			if (tally == boardSize) {
				return PlayerEnum.getByToken(playerToken);
			}
		}

		// check columns
		for (int col = 0; col < boardSize; ++col) {
			tally = 1;
			playerToken = board[0][col];
			if (playerToken == ' ') {
				continue;
			}

			for (int row = 1; row < boardSize; ++row) {
				if (board[row][col] == playerToken) {
					++tally;
				}
			}
			if (tally == boardSize) {
				return PlayerEnum.getByToken(playerToken);
			}
		}

		// check diag 1
		tally = 1;
		playerToken = board[0][0];
		if (playerToken != ' ') {
			for (int index = 1; index < boardSize; ++index) {
				if (board[index][index] == playerToken) {
					++tally;
				}

				if (tally == boardSize) {
					return PlayerEnum.getByToken(playerToken);
				}
			}
		}

		// check diag 2
		tally = 1;
		playerToken = board[0][boardSize - 1];
		if (playerToken != ' ') {
			for (int index = 1; index < boardSize; ++index) {
				if (board[index][boardSize - index - 1] == playerToken) {
					++tally;
				}
				if (tally == boardSize) {
					return PlayerEnum.getByToken(playerToken);
				}
			}
		}

		if (getAvailableCoords(startNode).size() == 0) {
			return PlayerEnum.DRAW;
		}

		return PlayerEnum.NOWINNER;
	}

	/*
	 * Set the player move and determine appropriate response.
	 * Player could win this move.
	 * Could be a draw this move.
	 * If neither, let the computer move.
	 * Computer could win.
	 * Could be a draw.
	 */
	public EngineResponse setPlayerMove(Coord coord) {

		if (!checkCoordLegal(coord)) {
			return new EngineResponse(EngineResponseEnum.ILLEGALCOORD);
		}
		/*
		 * Set the player move to the node that matches the coords that the user entered.
		 */
		for (Node node : lastMove.getChildNodes()) {
			if (node.getCoord().equals(coord)) {
				playerMove = node;
				lastMove = playerMove;
				break;
			}
		}
		printBoard(lastMove);

		// printChildren(lastMove);

		if (playerMove.hasWinner()) {
			// player wins
			return new EngineResponse(EngineResponseEnum.PLAYERWIN, playerMove);
		} else if (playerMove.getWinner() == PlayerEnum.DRAW) {
			return new EngineResponse(EngineResponseEnum.DRAW, playerMove);
		}

		// player did not win and board not yet filled
		computerMove();

		if (computerMove.hasWinner()) {
			// computer wins this move
			return new EngineResponse(EngineResponseEnum.COMPUTERWIN, computerMove);
		} else if (computerMove.getWinner() == PlayerEnum.DRAW) {
			// draw
			return new EngineResponse(EngineResponseEnum.DRAW, computerMove);
		}

		// computer did not win and no draw,
		// return the computer move coords
		return new EngineResponse(EngineResponseEnum.COMPUTERMOVE, computerMove);

	}

	/*
	 * Check if the move is legitimate: within board boundaries and coordinate is vacant.
	 */
	private boolean checkCoordLegal(Coord coord) {
		if (coord == null) {
			return false;
		}

		// check for coord within board size
		if (coord.getRow() >= boardSize || coord.getCol() >= boardSize || coord.getRow() < 0 || coord.getCol() < 0) {
			logger.debug("out of board size");
			return false;
		}

		// check that coord not already taken on previous moves
		if (lastMove == null) {
			// no moves have been made yet
			return true;
		}
		Node node = lastMove;
		logger.debug("node is " + node);
		logger.debug("lastMove is " + lastMove);

		while (node != headNode) {
			if (coord.equals(node.getCoord())) {
				logger.debug("coord not empty");
				return false;
			}
			node = node.getParent();
		}
		return true;

	}

	/*
	 * Return a string representation of the board.
	 */
	public String boardToString(Node node) {
		String boardString = "";

		char[][] board = generateBoard(node);

		String horizLine = "";
		for (int i = 0; i < boardSize; ++i) {
			horizLine += "----";
		}
		horizLine += "-";

		boardString += horizLine + " node: N" + node.getNodeNumber() + " player: " + node.getPlayer()
				+ System.lineSeparator();

		for (int i = 0; i < boardSize; ++i) {
			for (int j = 0; j < boardSize; ++j) {
				char s = ' ';
				if (board[i][j] != ' ') {
					s = board[i][j];
				}
				boardString += "| " + s + " ";
			}
			boardString += "|" + System.lineSeparator();
			boardString += horizLine + System.lineSeparator();
		}
		return boardString;

	}

	/*
	 * Print the board out in a human friendly fashion.
	 */
	void printBoard(Node node) {
		System.out.println(boardToString(node));
	}

	/*
	 * Create a 2D char array to hold the board.
	 * Put the X and O chars in according to moves that 
	 * computer and player have made.
	 */
	private char[][] generateBoard(Node startNode) {
		char[][] board = new char[boardSize][boardSize];
		for (int i = 0; i < boardSize; ++i) {
			board[i] = new char[boardSize];
			for (int j = 0; j < boardSize; ++j) {
				board[i][j] = ' ';
			}
		}

		// walk up the node tree,
		// remove any coord that exists in the node tree
		Node node = startNode;
		while (node != headNode) {
			board[node.getCoord().getRow()][node.getCoord().getCol()] = node.getPlayer().getToken();
			node = node.getParent();
		}

		return board;
	}

	/*
	 * Generate a list of available coords.
	 */
	private List<Coord> getAvailableCoords(Node startNode) {

		boolean[][] available = new boolean[boardSize][boardSize];
		for (int i = 0; i < boardSize; ++i) {
			available[i] = new boolean[boardSize];
			for (int j = 0; j < boardSize; ++j) {
				available[i][j] = true;
			}
		}

		// walk up the node tree,
		// remove any coord that exists in the node tree
		Node node = startNode;
		while (node != null && node.getCoord() != null) {
			available[node.getCoord().getRow()][node.getCoord().getCol()] = false;
			node = node.getParent();
		}

		List<Coord> availCoords = new ArrayList<Coord>();

		for (int i = 0; i < boardSize; ++i) {
			for (int j = 0; j < boardSize; ++j) {
				if (available[i][j]) {
					availCoords.add(boardCoords[i][j]);
				}
			}
		}

		return availCoords;
	}

	public Map<Integer, Node> getNodeMap() {
		return nodeMap;
	}

	public Node getHeadNode() {
		return headNode;
	}

	public boolean boardsEqual(Node node, char[][] b1) {
		char[][] b2 = generateBoard(node);
		return boardsEqual(b1, b2);
	}

	public boolean boardsEqual(char[][] b1, char[][] b2) {
		for (int i = 0; i < boardSize; ++i) {
			for (int j = 0; j < boardSize; ++j) {
				if (b1[i][j] != b2[i][j]) {
					return false;
				}
			}
		}
		return true;
	}

	/*
	 * 
	 * 
	 */
	public void findNodes(Node node, char[][] board, List<Node> nodeList) {
		if (boardsEqual(node, board)) {
			nodeList.add(node);
			return;
		}

		for (Node child : node.getChildNodes()) {
			findNodes(child, board, nodeList);
		}
	}
}
