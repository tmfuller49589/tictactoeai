package engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.Coord;
import model.Node;
import model.PlayerEnum;

/*
 * Class to play tic tac toe.
 * Board information is stored in a node.
 * Node contains a parent node and a list of child nodes.
 * 
 * All possible games are computed.  Every move and associated board position
 * is stored in a node.  By following the node children links, every possible
 * game can be replayed.
 * 
 * To avoid losing, we have to search through the tree and:
 * 1) make a forced move if required
 * 2) avoid making a move that leads to a position where two different moves can be made
 * to win the game.
 * 
 * With similar logic, computer will attempt to win if possible.
 * 
 */
public class EngineUgly extends EngineAbstract {

	/*
	 * Logger is a class for handling debugging and error messages.
	 * You can set the log level in the file src/main/resources/log4j2.xml,
	 * e.g. change
	 * <Logger name="tictactoe" level="info" additivity="false">
	 * to
	 * <Logger name="tictactoe" level="debug" additivity="false">
	 * to display debugging info.
	 */
	protected static final Logger logger = LogManager.getLogger(EngineUgly.class);

	public EngineUgly() {
	}

	@Override
	public void init() {
		super.init();

		computeForcedMoves(headNode);
		pruneComputerBadMoves(headNode);
		pruneForks();

	}

	/*
	 * 
	 * ============================================================================================================
	 * Code in this section is the logic for playing TicTacToe.
	 * ============================================================================================================
	 * 
	 */

	Node findSiblingByCoord(Node node, Coord coord) {

		for (Node n : node.getParent().getChildNodes()) {
			if (n.getCoord().equals(coord)) {
				return n;
			}
		}
		return null;
	}

	/*
	 * Go through the node tree of all games.
	 * Find the nodes that have a forced move.
	 * A force move is a move that has to be made to avoid loosing.
	 */
	void computeForcedMoves(Node node) {
		// does the next player have a winning move on the next turn?
		// compute forced moves for node passed to method
		node.getForcedMoves().clear();

		if (winOnNextMove(node) == null) {
			for (Node child : node.getChildNodes()) {
				for (Node grandChild : child.getChildNodes()) {
					if (grandChild.hasWinner() && grandChild.getWinner() == node.getPlayer()) {
						Node sibling = findSiblingByCoord(child, grandChild.getCoord());
						node.addForcedMove(sibling);
					}
				}
				// child has no children,
				// child is the last possible move
				// and so is a forced move
				if (child.getChildNodes().size() == 0) {
					node.addForcedMove(child);
				}
			}
		}

		// now compute forced moves for all children of node
		for (Node child : node.getChildNodes()) {
			computeForcedMoves(child);
		}
	}

	/*
	 * If there is a winning move next move,
	 * return the node.
	 */
	Node winOnNextMove(Node node) {
		for (Node child : node.getChildNodes()) {
			if (child.hasWinner()) {
				return child;
			}
		}
		return null;
	}

	/*
	 * Ugly.
	 */
	@Override
	void computerMove() {
		if (playerMove == null) {
			// player hasn't moved yet,
			playerMove = headNode;
		}

		logger.debug(playerMove.getChildNodes().size() + " moves to choose from");
		logger.debug("playerMove = " + playerMove);

		// can computer win next move?
		Node winNextMove = winOnNextMove(playerMove);
		if (winNextMove != null) {
			computerMove = winNextMove;
			lastMove = computerMove;
			return;
		}

		ArrayList<Node> candidateComputerMoveNodes = new ArrayList<Node>();
		for (Node node : playerMove.getChildNodes()) {
			if (!node.isPruned()) {
				candidateComputerMoveNodes.add(node);
			}
		}

		if (playerMove.getForcedMoves().size() == 1) {
			// one forced move, have to take it
			logger.info("one forced move");
			computerMove = playerMove.getForcedMoves().iterator().next();
			lastMove = computerMove;
			logger.info("computer move is " + computerMove);
			// printBoard(computerMove);
		} else if (playerMove.getForcedMoves().size() > 1) {
			// two forced move, computer lost
			logger.info("two forced moves, I quit");
			logger.info("last player move:");
			logger.info(playerMove);
			printBoard(playerMove);
			System.exit(0);
		} else {
			// no forced moves
			// look for a move that forces a win
			logger.info("no forced moves");
			computerMove = followForcedMoves(playerMove, PlayerEnum.COMPUTER);

			if (computerMove != null) {
				lastMove = computerMove;
				return;
			}

			// no force win
			// look for a forced draw
			computerMove = followForcedMoves(playerMove, PlayerEnum.DRAW);
			if (computerMove != null) {
				lastMove = computerMove;
				return;
			}
			if (candidateComputerMoveNodes.size() > 0) {
				// pick a random move from the list of candidates
				int randMove = randGen.nextInt(candidateComputerMoveNodes.size());
				computerMove = candidateComputerMoveNodes.get(randMove);
			} else {
				// If we get here, there is a logic bug
				logger.info("No non loosing moves. I quit, you win.");
				System.exit(0);
			}

			lastMove = computerMove;
		}
	}

	/*
	 * Set this node's pruned flag to true, and 
	 * all of this node's descendants.
	 */
	void prune(Node node) {
		node.setPruned(true);

		for (Node child : node.getChildNodes()) {
			prune(child);
		}
	}

	/*
	 * Get ancestor of node at given level.
	 * 0 = node
	 * 1 = node.getParent();
	 * 2 = node.getParent().getParent()
	 */
	Node getAncestor(Node node, int level) {
		Node ancestor = node;
		for (int i = 0; i < level; ++i) {
			if (!ancestor.hasParent()) {
				return null;
			}
			ancestor = ancestor.getParent();
		}
		return ancestor;
	}

	/*
	 * Prune the moves that lead to forks.
	 * Nasty code.  I think there should be a better way.
	 * Start at a terminal node.
	 * Travel up the tree looking for forks.
	 * Note that if human moves in corner (move 1)
	 * computer takes middle top (move 2)
	 * game has force loose on move 7
	 * The above gives example of why we sometimes must travel 5 generations up the tree
	 * to find the losing move.  Game was lost on move 2: 7-2=5
	 */
	void pruneForks() {
		List<Node> nodeList = new ArrayList<Node>();
		getUnprunedTerminalNodes(headNode, nodeList);

		for (Node node : nodeList) {
			if (node.hasWinner() && node.getWinner() == PlayerEnum.PLAYER) {
				if (!(node.hasParent() && node.getParent().hasParent())) {
					return;
				}

				Node ancestor1 = node.getParent();
				Node ancestor2 = getAncestor(node, 2);

				if (!ancestor1.hasParent()) {
					return;
				}

				boolean forcedFork = false;
				if (ancestor2.getForcedMoves().size() == 2) {
					Node ancestor3 = getAncestor(node, 3);
					Node ancestor4 = getAncestor(node, 4);
					Node ancestor5 = getAncestor(node, 5);
					if (ancestor5 != null) {
						if (ancestor4.getForcedMoves().size() == 1) {
							Coord coord = ancestor4.getForcedMoves().iterator().next().getCoord();
							if (coord.equals(ancestor3.getCoord())) {
								prune(ancestor5);
								forcedFork = true;
							}
						}
					}
				}

				if (forcedFork == false && ancestor2.getForcedMoves().size() == 2) {
					// unforced fork
					prune(ancestor2.getParent());
				}
			}
		}
	}

	/*
	 * If computer needs to make a move to avoid loosing,
	 * prune off all other choices.
	 */
	void pruneComputerBadMoves(Node node) {
		if (node.getForcedMoves().size() == 1) {
			Node forcedNode = node.getForcedMoves().iterator().next();
			// prune all children except for forced move
			Iterator<Node> it = node.getChildNodes().iterator();
			while (it.hasNext()) {
				Node n = it.next();
				if (n.getNodeNumber() != forcedNode.getNodeNumber()) {
					prune(n);
				}
			}
		}

		for (Node child : node.getChildNodes()) {
			pruneComputerBadMoves(child);
		}
	}

	/*
	 * Follow the trail of forced moves.
	 * If there is a path of forced moves to a draw,
	 * return the drawn node.
	 * Otherwise return null.
	 */
	Node followForcedMoves(Node node, PlayerEnum gameResult) {

		Node tmpNode = node;
		logger.info("node " + tmpNode.getNodeNumber() + " forced moves size is " + tmpNode.getForcedMoves().size());

		while (tmpNode.getForcedMoves().size() >= 1) {
			tmpNode = tmpNode.getForcedMoves().iterator().next();
			System.out.println("forced node is " + tmpNode);
		}

		logger.info("node " + tmpNode.getNodeNumber() + " winner is " + tmpNode.getWinner());

		if (tmpNode.getWinner() == gameResult) {
			return tmpNode;
		}

		logger.info("returning null");
		return null;
	}

	/*
	 * Starting from node, follow the tree and return a 
	 * list of all possible ending nodes that are descendants
	 * of node.
	 */
	@Override
	void getTerminalNodes(Node node, List<Node> nodes) {
		if (node.getChildNodes().size() == 0) {
			nodes.add(node);
			return;
		}

		for (Node child : node.getChildNodes()) {
			getTerminalNodes(child, nodes);
		}
	}

	/*
	 * Starting from node, follow the tree and return a 
	 * list of all possible ending nodes that are descendants
	 * of node that have not been pruned.
	 */
	void getUnprunedTerminalNodes(Node node, List<Node> nodes) {
		if (node.isPruned()) {
			return;
		}
		if (node.getChildNodes().size() == 0) {
			nodes.add(node);
			return;
		}

		for (Node child : node.getChildNodes()) {
			if (!child.isPruned()) {
				getUnprunedTerminalNodes(child, nodes);
			}
		}
	}

	/*
	 * 
	 * ============================================================================================================
	 * End of section containing the logic for playing TicTacToe.
	 * ============================================================================================================
	 * 
	 */

}
