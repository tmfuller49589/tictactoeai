package clientserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ManualClient {
	private Socket s;
	private DataInputStream din;
	private DataOutputStream dout;
	private CommandIO commandIo;
	protected static final Logger logger = LogManager.getLogger(ManualClient.class);
	private Scanner keyboard = new Scanner(System.in);

	public static void main(String[] args) throws UnknownHostException, IOException {
		ManualClient client = new ManualClient();
		client.start();

	}

	public ManualClient() {
		try {
			s = new Socket("localhost", 3333);
			din = new DataInputStream(s.getInputStream());
			dout = new DataOutputStream(s.getOutputStream());
			commandIo = new CommandIO(din, dout);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void start() {
		CommandEnum commTokenRead = CommandEnum.NOOP;

		while (true) {
			// get command from keyboard
			String commandString = keyboard.nextLine();
			CommandEnum command = CommandEnum.getByName(commandString);

			switch (command) {
			case NOOP:
				break;
			case STARTGAME:
				break;
			case TERMINATE:
				break;
			case MOVE:
				break;
			case GETBOARD:
				break;

			}

		}

		try {
			dout.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
