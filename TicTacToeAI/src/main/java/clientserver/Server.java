package clientserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import engine.EngineUgly;
import engine.EngineResponse;
import engine.EngineResponseEnum;

public class Server {

	private ServerSocket ss;
	private Socket s;
	private DataInputStream din;
	private DataOutputStream dout;
	private CommandIO commandIo;
	protected static final Logger logger = LogManager.getLogger(Server.class);
	private EngineUgly engine;

	public static void main(String[] args) {
		Server server = new Server();
		server.start();
	}

	public Server() {
		engine = new EngineUgly();

		try {
			ss = new ServerSocket(3333);
			s = ss.accept();
			din = new DataInputStream(s.getInputStream());
			dout = new DataOutputStream(s.getOutputStream());
			commandIo = new CommandIO(din, dout);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void start() {

		CommandEnum command;
		poll: while (true) {
			command = commandIo.read();
			switch (command) {
			case STARTGAME:
				EngineResponse er = engine.startGame();
				if (er.getEngineResponseEnum() == EngineResponseEnum.COMPUTERFIRST) {
					commandIo.write(er.getEngineResponseEnum(), er.getNode().getCoord().toString());
				} else {
					commandIo.write(er.getEngineResponseEnum());
				}
				break;
			case MOVE:
				break;
			case TERMINATE:
				break poll;
			case GETBOARD:
				break;
			case NOOP:
				break;
			}
		}

		try {
			din.close();
			s.close();
			ss.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
