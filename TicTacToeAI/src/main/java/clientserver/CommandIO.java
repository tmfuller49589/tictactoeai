package clientserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import engine.EngineResponseEnum;
import model.PlayerEnum;

public class CommandIO {

	DataInputStream din;
	DataOutputStream dout;
	protected static final Logger logger = LogManager.getLogger(CommandIO.class);

	public CommandIO(DataInputStream din, DataOutputStream dout) {
		super();
		this.din = din;
		this.dout = dout;
	}

	public CommandEnum read() {
		CommandEnum command = CommandEnum.NOOP;
		try {
			String s = din.readUTF();
			logger.debug("read " + s);
			command = CommandEnum.getByName(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return command;
	}

	public PlayerEnum readPlayerToken() {
		try {
			String s = din.readUTF();
			logger.debug("read " + s);
			for (PlayerEnum p : PlayerEnum.values()) {
				if (p.toString().equals(s)) {
					return p;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void write(CommandEnum command) {
		try {
			logger.debug("writing " + command);
			dout.writeUTF(command.toString());
			dout.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(PlayerEnum player) {
		try {
			logger.debug("writing " + player);
			dout.writeUTF(player.toString());
			dout.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(EngineResponseEnum engineResponse, String string) {
		try {
			logger.debug("writing " + engineResponse);
			dout.writeUTF(engineResponse.toString());
			dout.flush();
			dout.writeUTF(string);
			dout.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(EngineResponseEnum engineResponse) {
		try {
			logger.debug("writing " + engineResponse);
			dout.writeUTF(engineResponse.toString());
			dout.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
