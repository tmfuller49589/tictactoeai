package clientserver;

public enum CommandEnum {
	NOOP, STARTGAME, TERMINATE, MOVE, GETBOARD;

	public static CommandEnum getByName(String s) {
		for (CommandEnum cmd : CommandEnum.values()) {
			if (cmd.name().equals(s)) {
				return cmd;
			}
		}
		return null;
	}
}
