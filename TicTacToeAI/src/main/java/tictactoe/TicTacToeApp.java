package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import engine.EngineMiniMax;
import engine.EngineResponse;
import engine.EngineResponseEnum;
import model.Coord;

public class TicTacToeApp {

	private Scanner keybd = new Scanner(System.in);
	private EngineMiniMax engine;
	protected static final Logger logger = LogManager.getLogger(TicTacToeApp.class);

	public static void main(String[] args) {
		TicTacToeApp ic = new TicTacToeApp();
		ic.start();

	}

	public TicTacToeApp() {
		engine = new EngineMiniMax();
	}

	private void start() {
		do {
			EngineResponse er = engine.startGame();

			// engine will respond with "COMPUTERFIRST" and give node, or "PLAYERFIRST"
			// either way, print out the board position

			do {
				System.out.println(er.getNode());

				System.out.println(er.getEngineResponseEnum());

				System.out.println(engine.boardToString(er.getNode()));

				do {
					Coord coord = getPlayerMove();
					er = engine.setPlayerMove(coord);
				} while (er.getEngineResponseEnum().equals(EngineResponseEnum.ILLEGALCOORD));

				// print out the board position after player move
				System.out.println(engine.boardToString(er.getNode()));

			} while (gameOn(er.getEngineResponseEnum()));
			logger.info("result is " + er.getEngineResponseEnum());
		} while (true);

	}

	private boolean gameOn(EngineResponseEnum engineResponse) {
		logger.info("engine response is " + engineResponse);
		if (engineResponse.equals(EngineResponseEnum.DRAW) || engineResponse.equals(EngineResponseEnum.PLAYERWIN)
				|| engineResponse.equals(EngineResponseEnum.COMPUTERWIN)) {
			return false;
		}
		return true;
	}

	/*
	 * Read the keyboard to get the player move, check that user entered digits.
	 */
	private Coord getPlayerMove() {
		System.out.println("enter row column");

		Coord coord = null;
		try {
			int row = keybd.nextInt();
			int col = keybd.nextInt();
			keybd.nextLine(); // consume the enter key
			coord = new Coord(row, col);
		} catch (InputMismatchException e) {
			// if we get here, user has entered a non digit
			System.out.println("Illegal character entered. Please enter numbers only.");
			keybd.nextLine();
		}
		return coord;
	}
}
