package model;

public class Coord {
	private int row;
	private int col;

	public Coord(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	@Override
	public String toString() {
		String s = "(" + row + "," + col + ")";
		return s;
	}

	public boolean equals(Coord coord) {
		if (row == coord.row && col == coord.col) {
			return true;
		}
		return false;
	}
}
