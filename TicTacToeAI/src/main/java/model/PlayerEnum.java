package model;

/*
 * Enum to store categorical values:
 * player, computer, draw, and no winner
 * Also stores a character for each category.
 * 
 */
public enum PlayerEnum {
	PLAYER('x'), COMPUTER('o'), DRAW('D'), NOWINNER('n');

	char token;

	PlayerEnum(char c) {
		token = c;
	}

	public char getToken() {
		return token;
	}

	/*
	 * Given a character, return the associated category (PLAYER, COMPUTER, etc)
	 */
	public static PlayerEnum getByToken(char c) {
		for (PlayerEnum player : PlayerEnum.values()) {
			if (player.getToken() == c) {
				return player;
			}
		}
		return null;
	}

	public static PlayerEnum getOppositePlayer(PlayerEnum player) {
		switch (player) {
		case COMPUTER:
			return PlayerEnum.PLAYER;
		case PLAYER:
			return PlayerEnum.COMPUTER;
		default:
			return PlayerEnum.NOWINNER;
		}
	}
}
