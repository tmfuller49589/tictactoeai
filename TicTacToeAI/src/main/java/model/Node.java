package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Node {

	private List<Node> childNodes = new ArrayList<Node>();
	private Node parent;
	private Coord coord;
	private PlayerEnum player;
	private static int nodeCount = 0;
	private int nodeNumber;
	private PlayerEnum winner = PlayerEnum.NOWINNER;
	private Set<Node> forcedMoves = new HashSet<Node>();
	private boolean pruned = false;
	private Integer value = 0;

	public Node() {
		parent = null;
		coord = null;
	}

	public Node(Node parent, PlayerEnum player, Coord coord) {
		this.parent = parent;
		parent.childNodes.add(this);

		this.player = player;
		this.coord = coord;
		++nodeCount;
		nodeNumber = nodeCount;
	}

	public Node(Node parent, PlayerEnum player, int x, int y) {
		this(parent, player, new Coord(x, y));
	}

	public boolean hasParent() {
		if (parent != null) {
			return true;
		}
		return false;
	}

	public List<Node> getChildNodes() {
		return childNodes;
	}

	public void setChildNodes(List<Node> childNodes) {
		this.childNodes = childNodes;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public Coord getCoord() {
		return coord;
	}

	public void setCoord(Coord coord) {
		this.coord = coord;
	}

	public PlayerEnum getPlayer() {
		return player;
	}

	public void setPlayer(PlayerEnum player) {
		this.player = player;
	}

	public int getNodeNumber() {
		return nodeNumber;
	}

	@Override
	public String toString() {
		String s = "Node: N" + getNodeNumber();
		if (parent != null) {
			s += System.lineSeparator() + "parent: N" + parent.getNodeNumber();
		} else {
			s += System.lineSeparator() + "parent: null";
		}
		s += System.lineSeparator() + "depth: " + getDepth();
		if (coord != null) {
			s += System.lineSeparator() + "coord: " + getCoord().toString();
		} else {
			s += System.lineSeparator() + "coord: null";
		}
		s += System.lineSeparator() + "player: " + getPlayer();
		s += System.lineSeparator() + "winner: -->" + getWinner() + "<--";
		s += System.lineSeparator() + "pruned: -->" + isPruned() + "<--";
		s += System.lineSeparator() + "forced moves: ";
		for (Node forcedMove : forcedMoves) {
			s += System.lineSeparator() + "       Forced Node N" + forcedMove.getNodeNumber() + " coord "
					+ forcedMove.getCoord().toString();
		}
		s += System.lineSeparator() + "Value: " + value;

		return s;
	}

	public int getDepth() {
		int depth = 0;
		Node node = parent;
		while (node != null) {
			++depth;
			node = node.parent;
		}
		return depth;
	}

	public PlayerEnum getWinner() {
		return winner;
	}

	public void setWinner(PlayerEnum winner) {
		this.winner = winner;
	}

	public boolean hasWinner() {
		// if winner is not empty,
		// we have a winner
		return (winner == PlayerEnum.PLAYER || winner == PlayerEnum.COMPUTER);
	}

	public boolean isDraw() {
		if (winner == PlayerEnum.DRAW && childNodes.size() == 0) {
			return true;
		}
		return false;
	}

	public Set<Node> getForcedMoves() {
		return forcedMoves;
	}

	public void addForcedMove(Node node) {
		forcedMoves.add(node);
	}

	public boolean isPruned() {
		return pruned;
	}

	public void setPruned(boolean pruned) {
		this.pruned = pruned;
	}

	public static void setNodeCount(int nodeCount) {
		Node.nodeCount = nodeCount;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
