package engine;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import model.Node;

class EngineTest {
	protected static final Logger logger = LogManager.getLogger(EngineTest.class);
	static EngineMiniMax engine;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		engine = new EngineMiniMax();
		engine.init();

	}

	@BeforeEach
	void setUp() throws Exception {
	}

	// @Test
	void testBoardMatch() {
		//@formatter:off
		char b1[][] = { 
				{ 'x', 'o', 'x' }, 
				{ 'o', 'o', 'x' }, 
				{ 'o', 'x', 'o' } };
		//@formatter:on

		List<Node> nodeList = new ArrayList<Node>();

		engine.findNodes(engine.headNode, b1, nodeList);
		System.out.println(nodeList.size() + " nodes match board");
		for (Node node : nodeList) {
			System.out.println("found board, node is " + node);
			engine.printBoard(node);
		}
	}

}
