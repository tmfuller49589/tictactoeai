package engine;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Node;

class EngineMiniMaxTest {
	static EngineMiniMax engine;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		engine = new EngineMiniMax();
		engine.init();
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void minimaxNodeTest() {
		Node node = engine.getNodeMap().get(253488);
		int score = engine.minimax(node, 0, true);

		System.out.println("=======================================");
		System.out.println(node);
		engine.printBoard(node);

		System.out.println("score is " + score);
	}

	// @Test
	void test() {

		List<Node> terminalNodes = new ArrayList<Node>();
		engine.getTerminalNodes(engine.getHeadNode(), terminalNodes);

		Node node = terminalNodes.get(6);
		// Node node = engine.getNodeMap().get(6);
		engine.printBoard(node);

		System.out.println(node);

	}

	// @Test
	void printSiblings() {
		Node node = engine.getNodeMap().get(7);

		for (Node sib : node.getParent().getChildNodes()) {
			System.out.println("=======================================");
			System.out.println(sib);
			engine.printBoard(sib);
		}
	}

}
