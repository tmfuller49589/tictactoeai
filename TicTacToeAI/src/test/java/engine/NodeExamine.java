package engine;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Node;

class NodeExamine {
	protected static final Logger logger = LogManager.getLogger(NodeExamine.class);
	static EngineMiniMax engine;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		engine = new EngineMiniMax();
		engine.init();

	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testBlank() {

	}

	@Test
	void printChildren() {

		Node node = engine.getNodeMap().get(253372);

		System.out.println("=======================================");
		System.out.println(node);
		printChildren(node);

	}

	// @Test
	void findTerminalWithAncestor() {

		Node node = engine.getNodeMap().get(30615);
		ArrayList<Node> list = new ArrayList<Node>();
		engine.getTerminalNodes(node, list);

		for (Node n : list) {
			System.out.println(n);
			engine.printBoard(n);
		}

	}

	// @Test
	void printParents() {
		// Node node = engine.getNodeMap().get(24050);
		Node node = engine.getNodeMap().get(22);

		while (node.hasParent() == true) {
			System.out.println("=======================================");
			System.out.println(node);
			engine.printBoard(node);
			node = node.getParent();
		}
	}

	// @Test
	void printSiblings() {
		// Node node = engine.getNodeMap().get(24050);
		Node node = engine.getNodeMap().get(253115);

		for (Node sib : node.getParent().getChildNodes()) {
			System.out.println("=======================================");
			System.out.println(sib);
			engine.printBoard(sib);
		}
	}

	// @Test
	void printNode() {

		Node node = engine.getNodeMap().get(253115);
		System.out.println("=======================================");
		System.out.println(node);
		engine.printBoard(node);

	}

	// @Test
	void testTerminalNodes() {

		List<Node> terminals = new ArrayList<Node>();
		Node node = engine.getNodeMap().get(30330);

		engine.getTerminalNodes(node, terminals);

		System.out.println("============= printing terminal nodes ==========================");
		for (Node nod : terminals) {
			engine.printBoard(nod);
		}

	}

	/*
	 * Print out the node and board position for node.
	 * Also print out the child nodes and child board positions.
	 * Method is not currently called - used for debugging.
	 */
	void printChildren(Node node) {
		System.out.println("/////////// printing children ///////////////////////");
		System.out.println("parent: N" + node.getNodeNumber());
		engine.printBoard(node);
		for (Node child : node.getChildNodes()) {
			System.out.println("//////////////////////////////////");
			System.out.println(child);
			engine.printBoard(child);
			System.out.println("//////////////////////////////////");
		}
		System.out.println("/////////// done printing children ///////////////////////");
	}

}
