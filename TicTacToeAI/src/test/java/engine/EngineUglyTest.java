package engine;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Node;
import model.PlayerEnum;

class EngineUglyTest {
	protected static final Logger logger = LogManager.getLogger(EngineUglyTest.class);
	static EngineUgly engine;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		engine = new EngineUgly();
		engine.init();

	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testBlank() {

	}

	// @Test
	void findTerminalWithAncestor() {

		Node node = engine.getNodeMap().get(30615);
		ArrayList<Node> list = new ArrayList<Node>();
		engine.getTerminalNodes(node, list);

		for (Node n : list) {
			System.out.println(n);
			engine.printBoard(n);
		}

	}

	// @Test
	void testPrune() {

		engine.init();
		Node node = engine.getNodeMap().get(24409);
		System.out.println(node);
		engine.printBoard(node);
		engine.prune(node);

		while (node.hasParent() == true) {
			System.out.println("=======================================");
			System.out.println(node);
			engine.printBoard(node);
			node = node.getParent();
		}

	}

	// @Test
	void computeForcedNodes() {

		Node node = engine.getNodeMap().get(7337);
		engine.computeForcedMoves(node);
		System.out.println("=======================================");
		System.out.println(node);
		engine.printBoard(node);

	}

	// @Test
	void printUnprunedTerminalNodes() {

		Node node = engine.getNodeMap().get(2371);

		List<Node> nodeList = new ArrayList<Node>();
		engine.getUnprunedTerminalNodes(node, nodeList);
		System.out.println(nodeList.size() + " unpruned terminal nodes");
		for (Node n : nodeList) {
			System.out.println("=======================================");
			System.out.println(n);
			engine.printBoard(n);
		}
	}

	// @Test
	void printAncestor() {
		Node node = engine.getNodeMap().get(3334);
		for (int i = 0; i <= 5; ++i) {
			Node ancestor = engine.getAncestor(node, i);
			System.out.println("========  ancestor " + i + " ===============================");
			System.out.println(ancestor);
			engine.printBoard(ancestor);
		}

	}

	@Test
	void printNode() {

		Node node = engine.getNodeMap().get(6);
		System.out.println("=======================================");
		System.out.println(node);
		engine.printBoard(node);

	}

	// @Test
	void testTerminalNodes() {

		List<Node> terminals = new ArrayList<Node>();
		Node node = engine.getNodeMap().get(30330);

		engine.getTerminalNodes(node, terminals);

		System.out.println("============= printing terminal nodes ==========================");
		for (Node nod : terminals) {
			engine.printBoard(nod);
		}

	}

	// @Test
	void printForcedMoves() {

		Node node = engine.getNodeMap().get(30330);
		System.out.println(node);
		engine.printBoard(node);

		System.out.println("++++++++++++++++++++++++++++++++++++++++");

		engine.computeForcedMoves(node);

		System.out.println("----------------  printing forced move nodes");
		for (Node forced : node.getForcedMoves()) {
			System.out.println(forced);
			engine.printBoard(forced);
			System.out.println("----------------");
		}

	}

	// @Test
	void testForcedDraw() {

		Node node = engine.getNodeMap().get(30330);
		System.out.println(node);

		Node forcedEnd = engine.followForcedMoves(node, PlayerEnum.DRAW);

		System.out.println("============= printing forced end node ==========================");

		System.out.println("forced end is " + forcedEnd);
		if (forcedEnd != null) {
			engine.printBoard(forcedEnd);
		}
	}

	// @Test
	void testWinNextMove() {

		Node node = engine.getNodeMap().get(7337);
		Node winNext = engine.winOnNextMove(node);
		System.out.println(winNext);
		engine.printBoard(winNext);
	}
}
